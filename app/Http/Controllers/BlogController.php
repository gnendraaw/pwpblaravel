<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $blogs = Blog::where('user_id', request()->user()->id)->get();

        return view('blog/index', [
            'blogs' => $blogs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate(
            [
                'title' => 'required|min:8|max:255',
                'description' => 'required|min:8|max:1024'
            ],
        );
        $validate['user_id'] = auth()->user()->id;
        $validate['slug'] = $request->slug;
        $validate['created_at'] = today();
        $validate['updated_at'] = today();

        Blog::create($validate);

        return redirect('/blogs')->with('success', 'New blog created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);

        return view('blog/detail', [
            'blog' => $blog
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);

        return view('blog/edit', [
            'blog' => $blog,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate(
            [
                'title' => 'required|min:8|max:255',
                'description' => 'required|min:8|max:1024'
            ],
        );
        
        $validate['user_id'] = auth()->user()->id;
        $validate['slug'] = $request->slug;
        $validate['updated_at'] = today();

        Blog::where('id', $id)->update($validate);

        return redirect('/blogs')->with('success', 'blog updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::destroy($id);

        return redirect('/blogs')->with('success', 'blog deleted!');
    }
}
