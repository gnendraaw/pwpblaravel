@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header justify-content-between align-items-center d-flex">
                        {{ __('Blog') }}
                        <a href="/blogs/create" class="btn btn-dark">Create post</a>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($blogs as $blog)
                        <tr>
                            <th>{{$loop->iteration}}</th>
                            <td>{{$blog->title}}</td>
                            <td>{{$blog->slug}}</td>
                            <td>
                                    <form action="/blogs/{{$blog->id}}" method="post">
                                <div class="btn-group">
                                    <a href="/blogs/{{$blog->id}}" class="btn btn-dark">View</a>
                                    <a href="/blogs/{{$blog->id}}/edit" class="btn btn-warning">
                                        Edit
                                    </a>
                                        @method('DELETE')
                                        @csrf

                                        <button type="submit" name="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
