@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="mb-3">
                <a href="/" class="fw-bold text-primary text-decoration-none">Back to all blogs</a>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <img src="http://placekitten.com/1000/500" alt="" class="img-fluid">
                    </div>

                    <div class="mb-3">
                        <p class="h2 fw-bold">{{$blog->title}}</p>
                        <p class="">Uploaded by: <span class="text-primary fw-bold">{{$blog->user->name}}</span></p>
                        <p class="">{{$blog->description}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection