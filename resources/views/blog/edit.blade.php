@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <p class="h4 fw-bold">Update blog</p>
            <div class="card">
                <div class="card-body">
                    <form action="/blogs/{{$blog->id}}" method="POST">
                        @method('PUT')
                        @csrf

                        <div class="mb-3">
                            <label for="title">Title</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" value="{{$blog->title, old('title')}}" required>
                            @error('title')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="slug">Slug</label>
                            <input type="text" class="form-control" name="slug" id="slug" value="{{$blog->slug}}" required>
                        </div>
                        <div class="mb-3">
                            <label for="Description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror">{{$blog->description, old('description')}}</textarea>
                            @error('description')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <a href="/blogs" class="btn btn-outline-dark">Back</a>
                            <button type="submit" name="submit" class="btn btn-dark">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection