@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="" method="post">
                @csrf

                <div class="row justify-content-between mb-3">
                    <div class="col">
                        <input type="text" class="form-control" name="search" id="search">
                    </div>
                    <div class="col">
                        <input type="submit" class="btn btn-dark" value="search">
                    </div>
                </div>
            </form>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @foreach ($blogs as $blog)
                        <div class="col-sm-3 my-3">
                            <a href="blogs/{{$blog->id}}" class="text-decoration-none">
                                <div class="card">
                                    <img src="http://placekitten.com/100/60" alt="" class="rounded img-fluid">
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <p class="h5 fw-bold m-0 text-dark">{{$blog->title}}</p>
                                            <p class="text-primary m-0">{{$blog->user->name}}</p>
                                            <p class="text-secondary">{{$blog->created_at->diffForHumans()}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection