<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Blog;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Blog::create([
            'user_id' => 1,
            'title' => 'ini judul',
            'slug' => 'ini-slug',
            'description' => 'ini description',
            'created_at' => Date('Y-m-d'),
            'updated_at' => Date('Y-m-d'),
        ]);

        Blog::create([
            'user_id' => 1,
            'title' => 'ini judul',
            'slug' => 'ini-slug',
            'description' => 'ini description',
            'created_at' => Date('Y-m-d'),
            'updated_at' => Date('Y-m-d'),
        ]);

        Blog::create([
            'user_id' => 1,
            'title' => 'ini judul',
            'slug' => 'ini-slug',
            'description' => 'ini description',
            'created_at' => Date('Y-m-d'),
            'updated_at' => Date('Y-m-d'),
        ]);
    }
}
